// site information
site_name = 'The Loop';
site_desc = 'A bi-weekly meetup for developers to gather up, doing their open source project, help each other, and chat.';
site_logo = 'https://loopdev.community/images/logo.png';

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: site_name,
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
      },
      { hid: 'description', name: 'description', content: site_desc },
      { property: 'og:type', content: 'website' },
      { property: 'og:site_name', content: site_name },
      { property: 'og:title', content: site_name },
      { property: 'og:description', content: site_desc },
      { property: 'og:image', content: site_logo },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://rawgit.com/w3c/IntersectionObserver/master/polyfill/intersection-observer.js' }
    ],
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      /*
      ** Run scss loader
      */
      config.module.rules.push({
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      })
    }
  }
}
